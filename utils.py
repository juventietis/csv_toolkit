import csv
import sys
import os

def read_file(filename, delimiter="|"):
    file_obj = open(filename, "r", newline='')
    return csv.reader(file_obj, delimiter="|"), file_obj


def get_row_count(file_cursor):
    '''Get the row count in given file'''
    count = sum(1 for line in file_cursor)
    file_cursor = file_cursor.seek(0)
    return count


def get_header(csv_reader, file_cursor):
    '''Get the headers'''
    file_cursor.seek(0)
    header = next(csv_reader)
    return header
