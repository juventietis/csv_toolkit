from behave import given, when, then, step
from hamcrest import assert_that, equal_to, contains_string
import utils

@given('file {filename}')
def step_impl(context, filename):
    context.filename = filename

@when('we summarise the file')
def step_impl(context):
    csv_reader, file_obj = utils.read_file(context.filename)
    context.row_number = utils.get_row_count(file_obj)
    header = utils.get_header(csv_reader, file_obj)
    context.header = ",".join(header)
@then('the row count is {number:d}')
def step_impl(context, number):
    assert_that(context.row_number, equal_to(number))

@then(u'the columns names are: {cols}')
def step_impl(context, cols):
    expected_header = cols
    assert_that(context.header, contains_string(expected_header))

