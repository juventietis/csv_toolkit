from behave import given, when, then, step
from hamcrest import assert_that, equal_to

import utils

@given(u'the file {filename}')
def step_impl(context, filename):
    context.filename = filename
        

@when(u'read the file')
def step_impl(context):
    context.contents = utils.read_file(context.filename)[1].read()


@then(u'the contents are')
def step_impl(context):
    print(context.text)
    assert_that(context.contents, equal_to(context.text))
