Feature: Getting summary of the file
    Scenario: Get row count in the file
        Given file features/test.csv
        When we summarise the file
        Then the row count is 2
            
    Scenario: Get the column names
        Given file features/test.csv
        When we summarise the file
        Then the columns names are: col1,col2
